import Vue from 'vue'
import Vuex from 'vuex';
import Vuelidate from 'vuelidate'
import Toasted from 'vue-toasted';

import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store/index';

Vue.config.productionTip = false;

Vue.use(Toasted);
Vue.use(Vuex);
Vue.use(Vuelidate)


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
