import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Movie from '../views/Movie.vue'
import Profile from '../views/Profile.vue'
import Dashboard from '../views/Dashboard.vue'


import store from '../store';

Vue.use(VueRouter)

const isAdmin = (to, from, next) => {
  if (store.getters.getUser.role === "admin") {
    return next();
  }
  return next('/');
};

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue')
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/movie/:id",
    name: "Movie",
    component: Movie,
    props: true
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard,
    props: true,
    meta: {
      requiresAuth: true,
    },
    beforeEnter: isAdmin, 
  },
  {
    path: '*',
    name: 'Error',
    component: () => import('../views/Error.vue')
}
]

const router = new VueRouter({
  mode: 'history',
  routes
})


router.beforeEach((to, from, next) => {
  const authenticatedUser = store.getters.isAuthenticated;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  if(store.getters.isAdmin) {
    console.log(store.getters.isAdmin)
    next({ path: '/dashboard'});       
  }
  // Check for protected route
  if (requiresAuth && !authenticatedUser) next({ path: '/login' })
  else next();
 
});


export default router
