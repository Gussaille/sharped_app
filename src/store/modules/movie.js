export default {
    state: {
        movieList: [],
        movie: undefined,
    },
    
    getters: {
        movieList: state => state.movieList,
        movie: state => state.movie
    },

    mutations: {
        MOVIE_LIST(state, movieList){ state.movieList = movieList },
        MOVIE(state, movie){ state.movie = movie },
    },

    actions: {
        getMovies(context){
            fetch( `${process.env.VUE_APP_API_URL}/api/movie`, {
                method: "GET"
            })
            .then( response => {
                return !response.ok ? console.log(response) : response.json(response)
            })
            .then( async apiResponse => {
                context.commit('MOVIE_LIST', { data: apiResponse.movies })
            })
            .catch( apiError => console.log(apiError))
        },

        getMovie(context){
            let url = document.URL;
            const movieId = url.split("/").pop();

            fetch( `${process.env.VUE_APP_API_URL}/api/movie/${movieId}`, {
                method: "GET"
            })
            .then( response => {
                return !response.ok
                ? console.log(response)
                : response.json(response)
            })
            .then( async apiResponse => {
                context.commit('MOVIE', { data: apiResponse.data })
            })
            .catch( apiError => console.log(apiError))
        },
    }
}
