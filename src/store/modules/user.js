export default {
    namespaced: true,
    
    state: {
        users: [],
        user: undefined,
    },
    
    getters: {
        getUsers: state => state.users,
        getUser: state => state.user,
    },

    mutations: {
        SET_USERS (state, users) {
            state.users = users
        },
        SET_USER (state, user) {
            state.user = user
        },
        DELETE_USER(state, user){
            console.log(user._id)
            let index = state.users.data.findIndex(u => u._id === user._id)
            state.users.data.splice(index, 1)
        }
    },

    actions: {
        fetchUsers(context){
            fetch( `${process.env.VUE_APP_API_URL}/api/user`, {
                method: "GET"
            })
            .then( response => {
                return !response.ok ? console.log(response) : response.json(response)
            })
            .then( async apiResponse => {
                context.commit('SET_USERS', { data: apiResponse.data })
            })
            .catch( apiError => console.log(apiError))
        },

        fetchUser(context, id){
            fetch( `${process.env.VUE_APP_API_URL}/api/user/${id}`, {
                method: "GET"
            })
            .then( response => {
                return !response.ok ? console.log(response) : response.json(response)
            })
            .then( async apiResponse => {
                context.commit('SET_USER', { data: apiResponse.data })
            })
            .catch( apiError => console.log(apiError))
        },

        editUser(context, data){
            let id = data._id;

            fetch( `${process.env.VUE_APP_API_URL}/api/user/${id}`, {
                method: "PUT", 
                body: JSON.stringify({ 
                    givenName: data.givenName, 
                    familyName: data.familyName, 
                    image: data.image
                }),
                headers: {
                    'Content-Type': 'application/json'
                },
                credentials: 'include'
            }) 
            .then( response => !response.ok ? console.log(response) : response.json(response)) //=> Check response
            .then( async (apiResponse) =>await context.commit('SET_USER', { data: apiResponse.data })) //=> Commit changes
            .catch( apiError => console.log(apiError)) //=> Catch error
        },

        deleteUser(context, id){
            fetch( `${process.env.VUE_APP_API_URL}/api/user/${id}`, {
                method: "DELETE", 
                body: JSON.stringify({ id : id }),
                headers: {
                    'Content-Type': 'application/json'
                },
                credentials: 'include'
            }) 
            .then( response => !response.ok ? console.log(response) : response.json(response)) //=> Check response
            .then( async (apiResponse) => await context.commit('DELETE_USER', { data: apiResponse.data })) //=> Commit changes
            .catch( apiError => console.log(apiError)) //=> Catch error
        }
    }
}
