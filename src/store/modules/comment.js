export default {
    state: {
        comments: [],
        comment: null,
    },
    
    getters: {
        comments: state => state.comments
    },

    mutations: {
        COMMENTS(state, comments){ state.comments = comments },
        COMMENT(state, comment){ state.comment = comment },

    },

    actions: {
        getComments(context){
            fetch( `${process.env.VUE_APP_API_URL}/api/comment`, {
                method: "GET"
            })
            .then( response => {
                return !response.ok ? console.log(response) : response.json(response)
            })
            .then( async apiResponse => {
                context.commit('COMMENTS', { data: apiResponse.data })
            })
            .catch( apiError => console.log(apiError))
        },

        postComment(context, data){
            fetch(`${process.env.VUE_APP_API_URL}/api/comment`, {
                method: "POST",
                body: JSON.stringify({
                    title: data.title, 
                    content: data.content, 
                    movieId: data.movieId
                }),
                headers: {
                    'Content-Type': 'application/json'
                },
                credentials: 'include'
            })
            .then( response => {
                return !response.ok ? console.log(response) : response.json(response)
            })
            .then( async apiResponse => {
                context.commit('COMMENT', { data: apiResponse.data })
            })
            .catch( apiError => console.log(apiError))
        }
    }
}
