export default {
    state: {
        user: undefined,
    },
    
    getters: {
        getUser: (state) => state.user,
        isAuthenticated: (state) => !!state.user,
    },

    mutations: {
        USER( state, payload ){ state.user = payload.data },
        // AUTH( state, payload ){ state.user = payload.data },
    },

    actions: {
        checkUser(context){
            fetch( `${process.env.VUE_APP_API_URL}/auth/me`, { method: 'GET', credentials: 'include'})
            .then(async response => {
            if(!response.ok){
                console.log(response)
                await context.commit('USER', { data: undefined })
                // await context.commit('AUTH', { data: false })
            }
                else{ return response.json(response) } 
            })
            .then(async apiResponse => {
                if( apiResponse !== undefined ){
                    await context.commit('USER', { data: apiResponse })
                    // await context.commit('AUTH', { data: false }) 
                } else{
                    await context.commit('USER', { data: undefined })
                    // await context.commit('AUTH', { data: false })
                }   
            })
            .catch( apiError => console.log(apiError)) 
        },

        registerUser(context, data){
            fetch( `${process.env.VUE_APP_API_URL}/auth/register`, {
                method: `POST`,
                body: JSON.stringify({ 
                    givenName: data.givenName, 
                    familyName: data.familyName, 
                    email: data.email, password: 
                    data.password 
                }),
                headers: {
                    'Content-Type': 'application/json'
                },
                credentials: 'include'
            })
            .then( response => !response.ok ? console.log(response) : response.json(response)) 
            .then( async apiResponse => await context.commit('USER', { data: apiResponse.data })) 
            .catch( apiError => console.log(apiError)) 
        },

        logUser(context, data){
            fetch( `${process.env.VUE_APP_API_URL}/auth/login`, {
                method: 'POST',
                body: JSON.stringify({ email: data.email, password: data.password }),
                headers: {
                    'Content-Type': 'application/json'
                },
                credentials: 'include'
            })
            .then( response => !response.ok ? console.log(response) : response.json(response)) 
            .then( async apiResponse => {
                await context.commit('USER', { data: apiResponse.data }) 
                // await context.commit('AUTH', { data: true })
            })
            .catch( apiError => console.log(apiError)) 
        },

        logoutUser(context){
            fetch( `${process.env.VUE_APP_API_URL}/auth/logout`, { method: 'GET' })
            .then( response => !response.ok ? console.log(response) : response.json(response)) 
            .then( () => {
                context.commit('USER', { data: undefined })
                // await context.commit('AUTH', { data: false })
            }) 
            .catch( apiError => console.log(apiError)) 
        }
    }
}