import Vue from 'vue'
import Vuex from 'vuex'
import auth from "./modules/authentication";
import movie from "./modules/movie";
import user from "./modules/user";
import comment from "./modules/comment";

import createPersistedState from "vuex-persistedstate";



Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    movie,
    comment,
    user
  },
  plugins: [
    createPersistedState()
  ]
})
